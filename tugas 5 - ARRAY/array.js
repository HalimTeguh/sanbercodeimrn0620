var soal1 = "SOAL 1 - RANGE"
var soal2 = "SOAL 2 - RANGE WITH STEP"
var soal3 = "SOAL 3 - SUM OF RANGE"
var soal4 = "SOAL 4 - ARRAY MULTIDIMENSI"
var soal5 = "SOAL 5 - BALIK KATA"
var soal6 = "SOAL 6 - METODE ARRAY"

console.log(soal1)

function range(a, b) {
    if (a == null || b == null) return -1;

    var angka = []
    if (a < b) {
        for (i = a; i <= b; i++) {
            angka.push(i)
        }
    } else if (a > b) {
        for (i = a; i >= b; i--) {
            angka.push(i)
        }
    } else {
        angka += a
    }

    return angka
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(10, 1))
console.log(range(54, 50))
console.log(range())

console.log('')
console.log(soal2)
console.log('')

function rangeWithStep(a, b, c) {
    if (a == null || b == null || c == null) return -1

    var angka = []
    if (a < b) {
        for (i = a; i <= b; i += c) {
            angka.push(i)
        }
    } else if (a > b) {
        for (i = a; i >= b; i -= c) {
            angka.push(i)
        }
    } else {
        angka += a
    }
    return angka
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))


console.log(' ')
console.log(soal3)

function sum(a, b, c = 1 ) {
    if (a == null || b == null && c == null) {
        return 0
    } else if (a == null || b == null || c == null) {
        return 1
    }

    var angka = []
    if (a < b) {
        for (i = a; i <= b; i += c) {
            angka.push(i)
        }
    } else if (a > b) {
        for (i = a; i >= b; i -= c) {
            angka.push(i)
        }
    } else {
        angka += a
    }
    var total = 0

    for (i = 0; i < angka.length; i++) {
        total += angka[i]
    }
    return total
  
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(1))
console.log(sum())

console.log(' ')
console.log(soal4)

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(string) {
    var hasil = ''
    for (i = 0; i <= string.length - 1; i++) {
        
        console.log('Nomor ID : ' + string[i][0])
        console.log('Nama Lengkap : ' + string[i][1])
        console.log('TTL : ' + string[i][2] + ' - ' + string[i][3])
        console.log('Hobi : ' + string[i][4])
        console.log('')
    }
    return hasil
}
dataHandling(input)

console.log(' ')
console.log(soal5)

function baliKata(kata) {
    var hasil = ''
    for (i = kata.length-1; i >= 0; i--) {
        hasil = hasil+kata[i]
    }
    return hasil
}
console.log(baliKata("Kasur Rusak")) // kasuR rusaK
console.log(baliKata("SanberCode")) // edoCrebnaS
console.log(baliKata("Haji Ijah")) // hajI ijaH
console.log(baliKata("racecar")) // racecar
console.log(baliKata("I am Sanbers")) // srebnaS ma I 


console.log('')
console.log(soal6)
//["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
//["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(data) {
    data.splice(1, 1, 'Roman Alamsyah Elsharawy')
    data.splice(2, 1, 'Provinsi Bandar Lampung')
    data.splice(5, 0, 'Pria', 'SMA Internasional Metro')
    console.log(data)

    
    var bulan = '05'
    switch (bulan) {
        case '01': { console.log('Januari'); break; }
        case '02': { console.log('Februari'); break; }
        case '03': { console.log('Maret'); break; }
        case '04': { console.log('April'); break; }
        case '05': { console.log('Mei'); break; }
        case '06': { console.log('Juni'); break; }
        case '07': { console.log('Juli'); break; }
        case '08': { console.log('Agustus'); break; }
        case '09': { console.log('September'); break; }
        case '10': { console.log('Oktober'); break; }
        case '11': { console.log('November'); break; }
        case '12': { console.log('Desember'); break; }
    }
    var waktu = input[3]
    var tanggal = waktu.split('/')
    var dataTanggal = Number(tanggal[0])
    var dataBulan = Number(tanggal[1])
    var dataTahun = Number(tanggal[2])
    var dataTTL = dataTanggal + ' ' + dataBulan + ' ' + dataTahun
    var dataTTLsplit = dataTTL.split(' ')
    dataTTLsplit.sort(
        function (a, b) { return a > b }
    )
    console.log(dataTTLsplit)

    var dataTTLjoin = dataTTLsplit.join('-')
    console.log(dataTTLjoin)

    var dataNama = input[1]
    var dataNamaslice = dataNama.slice(0, 15)
    console.log(dataNamaslice)
    
}
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);
