var soal1 = 'soal 1 - Array to Object'
var soal2 = 'soal 2 - Shopping Time'
var soal3 = 'soal 3 - Naik Angkot'

console.log(soal1)
console.log('')

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini
    var yearkosong = 'Invalid Birth Year'
    if (arr[0] == null) return '" "'

    var data1 = {}
    
    data1.firstName = arr[0][0]
    data1.lastName = arr[0][1]
    data1.gender = arr[0][2]
    data1.age = []
    if (arr.length >= 3) {
        data1.age = yearkosong
    } else if (arr[0][3] < thisYear) {
        data1.age = (thisYear - arr[0][3])
    } else {
        data1.age = yearkosong
    }
    
  
    
    
    console.log('First Name : ' + data1.firstName)
    console.log('Last Name : ' + data1.lastName)
    console.log('Gender : ' + data1.gender)
    console.log('Age : ' + data1.age)
    console.log('')

    var data2 = {}
    data2.firstName = arr[1][0]
    data2.lastName = arr[1][1]
    data2.gender = arr[1][2]
    data2.age = []

    if ((arr[1][3] - thisYear) < thisYear) {
        data2.age = yearkosong
    } else if (arr[0][3] > thisYear) {
        data2.age = (thisYear - arr[1][3])
    } else {
        data2.age = yearkosong
    }
 


    console.log('First Name : ' + data2.firstName)
    console.log('Last Name : ' + data2.lastName)
    console.log('Gender : ' + data2.gender)
    console.log('Age : ' + data2.age)
    console.log('')
}


// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


console.log('')
console.log(soal2)
console.log('')

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var barang = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone']
    var harga = [1500000, 500000, 250000, 175000, 50000]
    var changeMoney = 0
    var hargabelanjaan = 0
    var beli = []
    if (memberId == "" || money == null) {
        return 'Mohon Maaf, toko X hanya berlaku untuk member saja'
    } else if (money <= 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        for (i = 0; i <= 5; i++) {
            if (money >= harga[i]) {
                hargabelanjaan = hargabelanjaan + harga[i]
                changeMoney = money - hargabelanjaan
                beli.push(barang[i])
            }
        }
        var databelajaan = {}
        databelajaan.memberId = memberId
        databelajaan.money = money
        databelajaan.listPurchased = beli
        databelajaan.changeMoney = changeMoney
    }
    return databelajaan

}
    // TEST CASES
    console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
    console.log(shoppingTime('82Ku8Ma742', 170000));
    //{ memberId: '82Ku8Ma742',
    // money: 170000,
    // listPurchased:
    //  [ 'Casing Handphone' ],
    // changeMoney: 120000 }
    console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
    console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



console.log('')
console.log(soal3)
console.log('')

function naikAngkot(arrPenumpang) {
    var arrobject = [];
    var lokasiAwal = []
    var lokasiAkhir = []
    var harga = 2000
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for (i = 0; i < arrPenumpang.length; i++) {
        lokasiAwal = rute.indexOf(arrPenumpang[i][1])
        lokasiAkhir = rute.indexOf(arrPenumpang[i][2])
        var dataPenumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: (lokasiAkhir - lokasiAwal) * harga
        }
        arrobject.push(dataPenumpang);
    }
    return arrobject;
}


//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]



