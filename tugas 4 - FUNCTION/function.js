var soal1 = "SOAL 1 - MEMBUAT FUNCTION MENGEMBALIKAN NILAI HALO SANBER!"
var soal2 = "SOAL 2 - MEMBUAT FUNCTION MENGEMBALIKAN HASIL NILAI PERKALIAN 2 PARAMETER"
var soal3 = "SOAL 3 - MEMBUAT FUNCTION MEMPROSES KALIMAT PERKENALAN"

console.log(soal1)

function teriak() {
    var string = 'Halo sanber!'
    return string
}

console.log(teriak())



console.log('')
console.log(soal2)

function kalikan(angka1 = 1, angka2 = 1) {
    var perkalian = angka1 * angka2
    return perkalian
}

var num1 = 50
var num2 = 2

var hasilkali = kalikan(num1, num2)
console.log(hasilkali)


console.log(' ')
console.log(soal3)

function introduce(name = budi , age = 20, address = jl.indonesia, hobby = ngoding) {
    var string = 'Hai, Nama saya ' + name + ', Umur saya ' + age + ' tahun , alamat tinggal saya ' + address + ', dan saya mempunya hobby yaitu ' + hobby + '.'
    return string
}

var nama = "agus"
var umur = "30"
var alamat = "Jln. Malioboro, yogyakarta"
var hobby = "Gaming" 

var perkenalan = introduce(nama, umur, alamat, hobby)
console.log(perkenalan)