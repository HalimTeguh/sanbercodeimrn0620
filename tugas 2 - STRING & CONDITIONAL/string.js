var soal1 = 'SOAL 1 - membuat kalimat'

var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

console.log(soal1);
console.log(word, second, third, fourth, fifth, sixth, seventh);
console.log(' ');


var soal2 = 'SOAL 2 - mengurai kalimat(string)'

var sentence = "I am going to be React Native Developer"

var firstword = sentence[0]
var secondword = sentence[2] + sentence[3];
var thirdword = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthword = sentence[11] + sentence[12];
var fifthword = sentence[14] + sentence[15];
var sixthword = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhword = sentence[22] + sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthword = sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log(soal2);
console.log('First Word: ' + firstword);
console.log('Second Word: ' + secondword);
console.log('Third Word: ' + thirdword);
console.log('Fourth Word: ' + fourthword);
console.log('Fifth Word: ' + fifthword);
console.log('Sixth Word: ' + sixthword);
console.log('Seventh Word: ' + seventhword);
console.log('Eighth Word; ' + eighthword)
console.log(' ');


var soal3 = 'Soal 3 - mengurai kalimat(substring)';

var sentence2 = 'Wow JavaScript is so cool';

var firstword2 = sentence2.substring(0, 3);
var secondword2 = sentence2.substring(4, 14);
var thirdword2 = sentence2.substring(15, 17);
var fourthword2 = sentence2.substring(18, 21);
var fifthword2 = sentence2.substring(21, 25);

console.log(soal3);
console.log('First Word: ' + firstword2);
console.log('Second Word: ' + secondword2);
console.log('Third Word: ' + thirdword2);
console.log('Fourth Word: ' + fourthword2);
console.log('Fifth Word: ' + fifthword2);
console.log(' ');


var soal4 = 'soal 4 - mengurai kalimat dan menentukan panjang string'

var sentence3 = 'Wow JavaScript is so cool';

var firstword3 = sentence3.substring(0, 3);
var secondword3 = sentence3.substring(4, 14);
var thirdword3 = sentence3.substring(15, 17);
var fourthword3 = sentence3.substring(18, 20);
var fifthword3 = sentence3.substring(21, 25);

console.log(soal4);
console.log('First Word: ' + firstword3 + ' - width lenght: ' + firstword3.length);
console.log('Second Word: ' + secondword3 + ' - width lenght: ' + secondword3.length);
console.log('Third Word: ' + thirdword3 + ' - width lenght: ' + thirdword3.length);
console.log('Fourth Word: ' + fourthword3 + ' - width lenght: ' + fourthword3.length);
console.log('Fifth Word: ' + fifthword3 + ' - width lenght: ' + fifthword3.length);
console.log(' ');