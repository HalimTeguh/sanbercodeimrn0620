//GAME WEREWOLF TUGAS 1 CONDITIONAL
var Nama = 'Halim'
var Peran = 'penyihir'

if (Nama == '' && Peran == '') {
    console.log('Nama harus diisi!')
} else if (Nama == Nama && Peran == '') {
    console.log('Halo ' + Nama + '!, Pilih Peran untuk memulai game!');

} else if (Nama == Nama && Peran == 'penyihir') {
    console.log('Selamat datang di Dunia Werewolf, ' + Nama)
    console.log('Halo ' + Peran + ' ' + Nama + '!, kamu dapat melihat siapa yang menjadi werewolf!')

} else if (Nama == Nama && Peran == 'guard') {
    console.log('Selamat datang di Dunia Werewolf, ' + Nama)
    console.log('Halo ' + Peran + ' ' + Nama + '!, kamu akan membantu melindungi temanmu dari serangan werewolf!')

} else if (Nama == Nama && Peran == 'werewolf') {
    console.log('Selamat datang di Dunia Werewolf, ' + Nama)
    console.log('Halo ' + Peran + ' ' + Nama + '!, kamu akan memakan mangsa setiap malam!')

} else {
    console.log('maaf ' + Nama + ', peran yang anda inginkan tidak ada!, silahkan pilih yang lain!')
}
console.log("...")


//TANGGAL TUGAS 2 CONDITIONAL

var hari = 31
var bulan = 11
var tahun = 2000


if (hari >= 1 && hari <= 31) {
    if (tahun >= 1900 && tahun <= 2200) {
        switch (bulan) {
            case 1: { console.log(hari + ' Januari ' + tahun); break }
            case 2: { console.log(hari + ' February ' + tahun); break }
            case 3: { console.log(hari + ' Maret ' + tahun); break }
            case 4: { console.log(hari + ' April ' + tahun); break }
            case 5: { console.log(hari + ' May ' + tahun); break }
            case 6: { console.log(hari + ' June ' + tahun); break }
            case 7: { console.log(hari + ' July ' + tahun); break }
            case 8: { console.log(hari + ' August ' + tahun); break }
            case 9: { console.log(hari + ' September ' + tahun); break }
            case 10: { console.log(hari + ' October ' + tahun); break }
            case 11: { console.log(hari + ' November ' + tahun); break }
            case 12: { console.log(hari + ' December ' + tahun); break }
            default: { console.log('input bulan salah') }
        }
    } else {
        console.log('input tahun salah')
    }
} else {
    console.log('input tanggal salah')
}