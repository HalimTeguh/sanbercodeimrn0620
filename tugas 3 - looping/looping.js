var tugas = "Tugas 3 - LOOPING"
var soal1 = "Soal 1 Looping dgn While"
var soal2 = "Soal 2 Looping dgn for"
var soal3 = "Soal 3 membuat persegi panjang #"
var soal4 = "Soal 4 membuat tangga"
var soal5 = "Soal 5 membuat papan catur"


console.log(tugas)
console.log(soal1)

var flag = 1
var teks1 = 'I love coding'

console.log(' ')
console.log('LOOPING PERTAMA')
while (flag <= 10 ) {
    console.log(2*flag + ' - ' + teks1)
    flag++
}
console.log('LOOPING KEDUA')
var flag2 = 10
var teks2 = 'I will become a mobile developer'

while (flag2 >= 1) {
    console.log(2*flag2 + ' - ' + teks2)
    flag2--
}

console.log(' ')
console.log(soal2)


for (var angka = 1; angka <= 20; angka += 1) {
    if (angka % 3 == 0 && angka % 2 == 1) {
        console.log(angka + ' - I love coding')
    } else if (angka % 2 == 0) {
        console.log(angka + ' _ berkualitas')
    } else {
        console.log(angka + ' - santai')
    }
}

console.log(' ')
console.log(soal3)

for (var pagar = 1; pagar <= 4; pagar++) {
    console.log("########" )
}

console.log(' ')
console.log(soal4)

var p = ''

for (var tangga = 1; tangga <= 7; tangga++) {
    console.log(p+='#')
}

console.log(' ')
console.log(soal5)

for (var catur = 1; catur <= 8; catur++) {
    if (catur % 2 == 0) {
        console.log(' # # # #')
    } else {
        console.log('# # # # ')
    }
}

