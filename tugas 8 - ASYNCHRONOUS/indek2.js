var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 

readBooksPromise(10000, books[0])
    .then(input => {
        readBooksPromise(input, books[1])
            .then(input => {
            readBooksPromise(input, books[2])
                    .then()
                    .catch(err => {console.log(err)})
        })
        .catch(err => {console.log(err)
    })
    })
    .catch(err => {
    console.log(err)
})