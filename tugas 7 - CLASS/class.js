var soal1 = 'SOAL 1 - ANIMAL CLASS'
var soal2 = 'SOAL 2 - FUNCTION TO CLASS'

console.log(soal1)
console.log('')

class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    present() {
        return this.name + ' adalah hewan berkaki ' + this.legs
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log('')


// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.legs = 2
    }
    yell() {
        return console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        
    }
    jump() {
        return console.log('hop hop')
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


console.log('')
console.log(soal2)
console.log('')

class Clock {
    // Code di sini
    constructor({ templates }) {
        this.templates = templates
        this.timer
    }
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.templates
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
    stop() {
        clearInterval(timer)
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000)
    }
}

var clock = new Clock({ templates: 'h:m:s' });
clock.start();  
