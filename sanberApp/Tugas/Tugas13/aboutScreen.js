import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <Text style={{ marginTop: 15, fontSize: 30, color: "#023e7d", fontWeight: "bold" }}>About Me</Text>
            <Image source={require('./image/johnnywang.jpg')} style={{ width: 200, height: 200, borderRadius: 300, marginTop: 15 }} />
            <Text style={{ marginTop: 15, fontSize: 25, color: "#023e7d", fontWeight: 'bold' }}>Halim Teguh S</Text>
            <Text style={{ marginTop: 5, fontSize: 15, color: "#3EC6FF", marginBottom: 10, fontWeight: 'bold'}}>React Native Developer</Text>
            <View style={styles.boxPorto}>
                <Text style={{ borderBottomWidth: 1, borderBottomColor: '#023e7d', paddingLeft: 5, textAlign: 'center', color:'#023e7d', fontWeight:'bold' }}>Portofolio</Text>
                <View style={styles.listPorto}>
                    <View style={styles.detilPorto}>
                        <AntDesign name="gitlab" size={34} color="#3EC6FF" />
                        <Text style={{ marginLeft: 10, color:'#023e7d', fontWeight:'bold' }}>Halim Teguh</Text>
                    </View>
                </View>
            </View>
            <View style={styles.boxKontak}>
                <Text style={{borderBottomWidth: 1,borderBottomColor:'#023e7d', paddingLeft: 5, marginBottom: 5, color:'#023e7d', fontWeight:'bold', textAlign:'center' }}>Hubungi Saya</Text>
                <View style={styles.listKontak}>
                    <View style={styles.detilKontak}>
                        <FontAwesome name="facebook-square" size={34} color="#3EC6FF" />
                        <Text style={{ marginLeft: 10, color:'#023e7d', fontWeight:'bold' }}>Halim Teguh</Text>
                    </View>
                    <View style={styles.detilKontak}>
                        <AntDesign name="instagram" size={34} color="#3EC6FF" />
                        <Text style={{ marginTop: 5, marginLeft: 10, color:'#023e7d', fontWeight:'bold' }}>@halimteguh_23</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    Text: {
        fontWeight: 'bold'

    },
    boxPorto: {
        display: 'flex',
        backgroundColor: "#EFEFEF",
        height: 100,
        width: 320,
        marginBottom: 10,
        borderRadius: 10,
        marginTop: 10
    },
    boxKontak: {
        display: 'flex',
        backgroundColor: "#EFEFEF",
        height: 120,
        width: 320,
        borderRadius: 10,
        marginTop: 10
    },
    listKontak: {
        display: 'flex',
        alignItems: 'center',
    },
    detilKontak: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 5
    },
    detilPorto: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 10
    },
    listPorto: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
