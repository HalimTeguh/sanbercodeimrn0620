import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default function LoginScreen() {

    return (
        <View style={styles.container}>
            <Image source={require('./image/logo.png')} style={{ width: 300, height: 100, marginTop: -100 }} />
            <Text style={{ margin: 15, fontSize: 20 }}>Login</Text>

            <view style={styles.inputText.container}>
             <Text style={{fontSize: 12, }}>Username/Email</Text>
              <View style={styles.inputText}>
                <TextInput placeholder="Input your email" style={{ fontSize: 13 }}></TextInput>
              </View>
              <Text style={{fontSize: 12, }}>Password</Text>
              <View style={styles.inputText}>
                 <TextInput placeholder="Input your password" style={{ fontSize: 13}}></TextInput>
              </View>
            </view>
            
            <TouchableOpacity style={styles.buttonMasuk}>
                <Text style={{ backgroundColor: "#3EC6FF", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 200 }}>Masuk</Text>
            </TouchableOpacity>
            <View style={{ marginTop: 15 }}>
                <Text>Atau</Text>
            </View>
            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={{ backgroundColor: "#003366", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 200 }}>Daftar ?</Text>
            </TouchableOpacity>
        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    text: {
        fontSize: 30, color: '#04063c',
    },
    inputText: {
        marginBottom: 8,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: "#003366",
        flexDirection: "row",
   

    },
    buttonMasuk: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
    },
    buttonDaftar: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    }
});
