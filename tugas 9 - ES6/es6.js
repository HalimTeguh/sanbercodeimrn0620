var soal1 = 'SOAL 1 - MENGUBAH FUNGSI MENJADI ARROW'
var soal2 = 'SOAL 2 - SEDERHANAKAN MENJADI OBJECT LITERAL DI ES6'
var soal3 = 'SOAL 3 - DESTRUCTURING'
var soal4 = 'SOAL 4 - ARRAY SPREADING'
var soal5 = 'SOAL 5 - TEMPLATE LITERALS'


console.log(soal1)


var goldenFunction = () => {
    return () => {
        console.log('this is golden!!')
    }
}

const golden = function goldenFunction() {
    console.log("this is golden!!")
}

golden()

console.log('')
console.log('')
console.log(soal2)


const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => { console.log(firstName + " " + lastName) }
    }
}



//Driver Code 
newFunction("William", "Imoh").fullName() 

console.log('')
console.log('')
console.log(soal3)

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

console.log('')
console.log('')
console.log(soal4)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

//Driver Code
console.log(combined)

console.log('')
console.log('')
console.log(soal5)

const planet = "earth"
const view = "glass"
var before = `lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)
console.log("\n")
